 //#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h"
const double realvalueofpi = 3.14159265359;
float pi_wallis_product_float  (int n){

  float numerator, pi_estimate, product;
  for (int i=0; i<n;i++) {
    numerator= 2*i;

    product= ((float)numerator/(float)(numerator+1))* ((float)numerator/(float)(numerator-1));
    //if condition to catch when i=0. 
    if (pi_estimate!=0){
        pi_estimate=product*pi_estimate;
    }else {
        pi_estimate=product;
    }
  }  
return pi_estimate*2;

}

double pi_wallis_product_double  (int n){

  double numerator, pi_estimate, product;
   //for loop for calculating sum
  for (int i=0; i<n;i++) {
    numerator= 2*i;

    product= ((double)numerator/(double)(numerator+1))* ((double)numerator/(double)(numerator-1));
    //if condition to calculate when i=0.
    if (pi_estimate!=0){
        pi_estimate=product*pi_estimate;
    }else {
        pi_estimate=product;
    }
  }  
return pi_estimate*2;

}


int main() {

//#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
  //  stdio_init_all();
//#endif
    //call float function to get value for pi.
  	float piFloat =pi_wallis_product_float(100000);
    //calculate error
    float piFloatError= realvalueofpi-piFloat;

    //call float function to get value for pi.
    double piDouble=pi_wallis_product_double(100000);
    double piDoubleError=realvalueofpi-piDouble;

    // Print a console message to give float value and error.
    printf("Value using Floats = %f, Error =%f\n",piFloat,piFloatError);

    // Print a console message to give double value and error.
    printf("Value using Doubles = %f, Error =%f\n",piDouble,piDoubleError);

    // Returning zero indicates everything went okay.
    return 0;
}

