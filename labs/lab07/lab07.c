

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.
#include "hardware/gpio.h"  // Required for usinf XIP
#include "hardware/address_mapped.h" // required for getting XIP_CTRL_BASE
#include "hardware/structs/xip_ctrl.h" // required for writing to the control base table  using xip_ctrl_hw
#include "hardware/regs/xip.h" //required for accesing the xip table
#include "hardware/timer.h" //timing
#include "hardware/regs/addressmap.h"
#include "hardware/regs/adc.h"




/*
 * @brief LAB #07 - TEMPLATE
 *        Main entry point for the code - calls the main assembly
 *        function where the body of the code is implemented.
 * 
 * @return int      Returns exit-status zero on completion.
 */


// Function to get the enable status of the XIP cache
bool get_xip_cache_en(){  // 0x00000001 [0] 
    if ( (xip_ctrl_hw->ctrl & 0x00000001) == 0x00000001) 
        return true ; 
    else
        return false ; 
}

// Function to set the enable status of the XIP cache 
bool set_xip_cache_en(bool cache_en){
    if (cache_en == true) //the first bit for the ctrl base enables  and disables caching
      xip_ctrl_hw->ctrl = (xip_ctrl_hw->ctrl | 0x00000001) ; // writes this bit as 1 to enable caching  
    else
      xip_ctrl_hw->ctrl = (xip_ctrl_hw->ctrl & 0xFFFFFFFE) ; // writes this bit as 0 to disbale caching  
  
    return 1 ; 
}


void core1_entry() {
    while (1) {
        
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

const double realvalueofpi = 3.14159265359;
int32_t pi_wallis_product_float  (int n){

  float numerator, pi_estimate, product;
  for (int i=0; i<n;i++) {
    numerator= 2*i;

    product= ((float)numerator/(float)(numerator+1))* ((float)numerator/(float)(numerator-1));
    //if condition to catch when i=0. 
    if (pi_estimate!=0){
        pi_estimate=product*pi_estimate;
    }else {
        pi_estimate=product;
    }
  }  
return pi_estimate*2;

}

int32_t pi_wallis_product_double  (int n){

  double numerator, pi_estimate, product;
   //for loop for calculating sum
  for (int i=0; i<n;i++) {
    numerator= 2*i;

    product= ((double)numerator/(double)(numerator+1))* ((double)numerator/(double)(numerator-1));
    //if condition to calculate when i=0.
    if (pi_estimate!=0){
        pi_estimate=product*pi_estimate;
    }else {
        pi_estimate=product;
    }
  }  
return pi_estimate*2;

}


int main() {

    //    Code for sequential run goes here…
    //    Take snapshot of timer and store
    //    Run the single-precision Wallis approximation
    //    Run the double-precision Wallis approximation
    //    Take snapshot of timer and store
    //    Display time taken for application to run in sequential mode

    const int    ITER_MAX   = 100000;
    stdio_init_all();
    multicore_launch_core1(core1_entry);


    //sequential

    set_xip_cache_en(1);
   int64_t timebefore1,timebefore,timeafter,floattime, doubletime,totaltime;
    //call float function to get value for pi.
    timebefore1=time_us_64();
    float piFloat =pi_wallis_product_float(100000);
    //calculate error
 //   float piFloatError= realvalueofpi-piFloat;
    timeafter=time_us_64();
    //timer for float function
    floattime=timeafter-timebefore1;

    //call float function to get value for pi.
    timebefore=time_us_64();
    double piDouble=pi_wallis_product_double(100000);
  //  double piDoubleError=realvalueofpi-piDouble;
    //timer for float function
    timeafter=time_us_64();
    doubletime=timeafter-timebefore;
    //timer for two events
    totaltime=floattime+doubletime;
    // Print a console message to give float value, error, and time.
    printf(" Scenerio 1: \n Time for float = %u,\n Time for Double =%u,\n Total time =%u \n",floattime, doubletime, totaltime);



    //scenerio 2 sequentail cache disabled
    set_xip_cache_en(0);
   
    
    //call float function to get value for pi.
    timebefore1=time_us_64();
     piFloat =pi_wallis_product_float(100000);
    //calculate error
    //piFloatError= realvalueofpi-piFloat;
    timeafter=time_us_64();
    //timer for float function
    floattime=timeafter-timebefore1;

    //call float function to get value for pi.
    timebefore=time_us_64();
    piDouble=pi_wallis_product_double(100000);
  //  piDoubleError=realvalueofpi-piDouble;
    //timer for float function
    timeafter=time_us_64();
    doubletime=timeafter-timebefore;
    //timer for two events
    totaltime=floattime+doubletime;
    // Print a console message to give float value, error, and time.
   printf(" Scenerio 2: \n Time for float = %u,\n Time for Double =%u,\n Total time =%u \n",floattime, doubletime, totaltime);



  
    
    //load floast function onto core1
    set_xip_cache_en(1);
    //call float function to get value for pi.
    timebefore1=time_us_64();
    multicore_fifo_push_blocking((uintptr_t) &pi_wallis_product_float);
    multicore_fifo_push_blocking(ITER_MAX);
     //call float function to get value for pi.
    timebefore=time_us_64();
    piDouble=pi_wallis_product_double(ITER_MAX);
    timeafter=time_us_64();
    doubletime=timeafter-timebefore;
    multicore_fifo_pop_blocking();
    //timer for float function
    timeafter=time_us_64();
    //timer for float function
    floattime=timeafter-timebefore1;

    //timer for two events
    totaltime=floattime+doubletime;
    // Print a console message to give float value, error, and time.
     printf(" Scenerio 3: \n Time for float = %u,\n Time for Double =%u,\n Total time =%u \n",floattime, doubletime, totaltime);


    set_xip_cache_en(0); //disable catch 

    //call float function to get value for pi.
    timebefore1=time_us_64();
    multicore_fifo_push_blocking((uintptr_t) &pi_wallis_product_float);
    multicore_fifo_push_blocking(ITER_MAX);
     //call float function to get value for pi.
    timebefore=time_us_64();
    piDouble=pi_wallis_product_double(ITER_MAX);
    timeafter=time_us_64();
    doubletime=timeafter-timebefore;
    multicore_fifo_pop_blocking();
    //timer for float function
    timeafter=time_us_64();
    //timer for float function
    floattime=timeafter-timebefore1;

    //timer for two events
    totaltime=floattime+doubletime;
    // Print a console message
     printf(" Scenerio 4: \n Time for float = %u,\n Time for Double =%u,\n Total time =%u \n",floattime, doubletime, totaltime);


    // Returning zero indicates everything went okay.
    return 0;
}
