#include "hardware/regs/addressmap.h"
#include "hardware/regs/m0plus.h"

.syntax unified                 @ Specify unified assembly syntax
.cpu    cortex-m0plus           @ Specify CPU type is Cortex M0+
.thumb                          @ Specify thumb assembly for RP2040
.global main_asm                @ Provide program starting address to the linker
.align 4                        @ Specify code alignment

.equ    SLEEP_TIME, 500         @ Specify the sleep time (in ms)
.equ    LED_GPIO_PIN, 25        @ Specify the pin that the LED is connected to
.equ    LED_GPIO_OUT, 1         @ Specify the direction of the GPIO pin
.equ    LED_VALUE_ON, 1         @ Specify the value that turns the LED "on"
.equ    LED_VALUE_OFF, 0        @ Specify the value that turns the LED "off"
.equ    SVC_ISR_OFFSET, 0x2C    @ The SVC is entry 11 in the vector table
.equ    SVC_MAX_INSTRS, 0x01    @ Maximum allowed SVC subroutines

@ Entry point to the ASM portion of the program
main_asm:
    bl      init_gpio_led       @ Initialise the GPIO LED pin
    bl      install_svc_isr     @ Install the SVC interrupt service routine
loop:
    svc     #0                  @ Call the SVC ISR with value 0 (turns on LED)
    nop                       @ Add a no-op instruction for alignment after SVC
    bl      do_sleep            @ Short pause before proceeding
    svc     #1                  @ Call the SVC ISR with value 1 (turns off LED)
    nop                         @ Add a no-op instruction for alignment after SVC
    bl      do_sleep            @ Add a short pause before proceeding
    b       loop                @ Always jump back to the start of the loop

@ Subroutine used to introduce a short delay in the application
do_sleep:
   @ <TODO – add assembly code to implement the sleep delay using sleep_ms>
    push    {lr}                @ <TODO – add comment>
    ldr  R0,=SLEEP_TIME            @ Set the value of Polling Intervals
    bl      sleep_ms 
    pop     {pc}                @ <TODO – add comment>

@ Subroutine used to initialise the PI Pico built-in LED
init_gpio_led:
    push    {lr}                @ Store the link register to the stack
    movs    r0, #LED_GPIO_PIN   @ This value is the GPIO BUTTON pin on the PI PICO board
    bl      asm_gpio_init       @ Call the subroutine to initialise the GPIO pin specified by r0
    movs    r0, #LED_GPIO_PIN   @ This value is the GPIO LED pin on the PI PICO board
    movs    r1, #LED_GPIO_OUT   @ We want this GPIO pin to be setup as an output pin
    bl      asm_gpio_set_dir    @ Call the subroutine to set the GPIO pin specified by r0 to state specified by r1
    pop     {pc}                @ Pop the link register from stack counter

@ Subroutine used to install the SVC interrupt service handler
install_svc_isr:
    ldr     r2, =(PPB_BASE + M0PLUS_VTOR_OFFSET)    @ Find the entry point of the SVC jump table and set as r2
    ldr     r1, [r2]                                @ Make r1 point to the location of entry point of the jump table
    movs    r2, #SVC_ISR_OFFSET                     @ Caluclate the offset of this particular interrupt
    add     r2, r1                                  @ Find the location of this interrupt in this table and store in r2
    ldr     r0, =svc_isr                            @ Load the entry point of this interrupt into r0
    str     r0, [r2]                                @ Set the table location of R0 as the value caluated in r2
    bx      lr                                      @ branch to the point at which the intterupt was called (stored in lr)

@ SVC interrupt service handler routine
.thumb_func                     @ Required for all interrupt service routines
svc_isr:
    push    {lr}                @ Store the link register to the stack
    ldr     r0, [sp, #0x1C]     @ Load the  the values within the range addresse of the stack pointer and #0x1C
    subs    r0, #0x2            @ Subtract two to align adresses
    ldr     r0, [r0]            @ updates the value of the address  of r0
    ldr     r1, =#0xFF          @ Initailised the value of xff into r1 to clear the 8 bit comment field to get svc routine number 
    ands    r0, r1              @ Get the value of the svc routine by and-ing and store that in r0
    cmp     r0, #SVC_MAX_INSTRS @ Set Condition flags by Comparing r0 to max svc subroutines
    bgt     svc_done            @ Branch if greater than prevents too many svc subroutines being written
    adr     r1, svc_jmptbl      @ Load the adresses of the subroutine
    lsls    r0, #2              @ Shift the value of r0 (svc routine number) to the left
    ldr     r1, [r1, r0]        @ Load the value of adresses of  into r1
    mov     pc, r1              @ update the program counter to the value of r1
svc_done:
    pop     {pc}                @ Pop the link register from the stack to the program counter

@ First function of SVC subroutine - turn on the LED
svc_num0:
   @ push    {lr}                @ Store the link register to the stack
    movs    r1, #LED_VALUE_ON   @ The LED is off so turn it on
    movs    r0, #LED_GPIO_PIN   @ Set the LED GPIO pin number to r0 for use by asm_gpio_put
    bl      asm_gpio_put        @ Update the the value of the LED GPIO pin (based on value in r1)
    @ pop     {pc}               @ Pop the link register from the stack to the program counter
    b       svc_done            @ Branch back to the main ISR when done

@ Second function of SVC subroutine - turn off the LED
svc_num1:
   @ push    {lr}                @ Store the link register to the stack
    movs    r1, #LED_VALUE_OFF  @ The LED is on so turn it off
    movs    r0, #LED_GPIO_PIN   @ Set the LED GPIO pin number to r0 for use by asm_gpio_put
    bl      asm_gpio_put        @ Update the the value of the LED GPIO pin (based on value in r1)
   @ pop     {pc}                @ Pop the link register from the stack to the program counter
    b       svc_done            @ Branch back to the main ISR when done

@ SVC function entry jump table.
.align 2
svc_jmptbl:
    .word svc_num0              @ Entry zero goes to SVC function #0.
    .word svc_num1              @ Entry one goes to SVC function #1.
    .word 0                     @ Null termination of the jump table.

@ Set data alignment
.data
    .align 4