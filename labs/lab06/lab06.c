#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.

/**
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on 
 *        the FIFO, which also indicates that the result is ready.
 */

void core1_entry() {
    while (1) {
        
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

const double realvalueofpi = 3.14159265359;
float pi_wallis_product_float  (int n){

  float numerator, pi_estimate, product;
  for (int i=0; i<n;i++) {
    numerator= 2*i;

    product= ((float)numerator/(float)(numerator+1))* ((float)numerator/(float)(numerator-1));
    //if condition to catch when i=0. 
    if (pi_estimate!=0){
        pi_estimate=product*pi_estimate;
    }else {
        pi_estimate=product;
    }
  }  
return pi_estimate*2;

}

double pi_wallis_product_double  (int n){

  double numerator, pi_estimate, product;
   //for loop for calculating sum
  for (int i=0; i<n;i++) {
    numerator= 2*i;

    product= ((double)numerator/(double)(numerator+1))* ((double)numerator/(double)(numerator-1));
    //if condition to calculate when i=0.
    if (pi_estimate!=0){
        pi_estimate=product*pi_estimate;
    }else {
        pi_estimate=product;
    }
  }  
return pi_estimate*2;

}


int main() {

    //    Code for sequential run goes here…
    //    Take snapshot of timer and store
    //    Run the single-precision Wallis approximation
    //    Run the double-precision Wallis approximation
    //    Take snapshot of timer and store
    //    Display time taken for application to run in sequential mode

    const int    ITER_MAX   = 100000;

    
  //  multicore_launch_core1(core1_entry);
   
    //sequential
    double timebefore1,timebefore,timeafter,floattime, doubletime,totaltime;
    //call float function to get value for pi.
    timebefore1=time_us_64();
    float piFloat =pi_wallis_product_float(100000);
    //calculate error
    float piFloatError= realvalueofpi-piFloat;
    timeafter=time_us_64();
    //timer for float function
    floattime=timeafter-timebefore1;

    //call float function to get value for pi.
    timebefore=time_us_64();
    double piDouble=pi_wallis_product_double(100000);
    double piDoubleError=realvalueofpi-piDouble;
    //timer for float function
    timeafter=time_us_64();
    doubletime=timeafter-timebefore;
    //timer for two events
    totaltime=time_us_64()-timebefore1;
    // Print a console message to give float value, error, and time.
    printf("Value using Floats = %f, Error =%f, time =%f \n",piFloat,piFloatError,floattime);

    // Print a console message to give double value, error and time.
    printf("Value using Doubles = %f, Error =%f, time =%f\n",piDouble,piDoubleError,doubletime);
    printf("totaltime for float and double =%f\n", totaltime);



  
    stdio_init_all();
    multicore_launch_core1(core1_entry);
    //load floast function onto core1
    multicore_fifo_push_blocking((uintptr_t) &pi_wallis_product_float);
    multicore_fifo_push_blocking(ITER_MAX);

  
  
    //call float function to get value for pi.
    timebefore1=time_us_64();
    piFloat =multicore_fifo_pop_blocking();
    //calculate error
    piFloatError= realvalueofpi-piFloat;
    timeafter=time_us_64();
    //timer for float function
    floattime=timeafter-timebefore1;

    //call float function to get value for pi.
    timebefore=time_us_32();
    piDouble=pi_wallis_product_double(ITER_MAX);

    piDoubleError=realvalueofpi-piDouble;
    //timer for float function
    timeafter=time_us_32();
    doubletime=timeafter-timebefore;
    //timer for two events
    totaltime=time_us_32()-timebefore1;
    // Print a console message to give float value, error, and time.
    printf("Value using Floats = %f, Error =%f, time =%f \n",piFloat,piFloatError,floattime);

    // Print a console message to give double value, error and time.
    printf("Value using Doubles = %f, Error =%f, time =%f\n",piDouble,piDoubleError,doubletime);
    printf("totaltime for float and double =%f", totaltime);

    // Returning zero indicates everything went okay.
    return 0;
}
